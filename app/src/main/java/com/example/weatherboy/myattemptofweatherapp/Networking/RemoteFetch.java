package com.example.weatherboy.myattemptofweatherapp.Networking;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.example.weatherboy.myattemptofweatherapp.CityPreference;
import com.example.weatherboy.myattemptofweatherapp.R;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;


/**
 * Created by nikola on 24.1.17..
 */

public class RemoteFetch implements Runnable, Response.Listener, Response.ErrorListener {

    private Activity mActivity;
    private Context mContext;

    public RemoteFetch(Context c, Activity a) {
        mContext = c;
        mActivity = a;
    }

    private static final String TAG = "RemoteFetch";

    //just in case method getURl fails so I can test with this one
    private static final String testURL = "https://api.darksky.net/forecast/5daba052d7cf1e56818f2a4226094d56/45.267135,19.833550";

    @Override
    public void run() {
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
        RequestQueue mQueue;
        JsonRequest mJSONrequest;
        mQueue = Volley.newRequestQueue(mContext.getApplicationContext());
        mJSONrequest = new JsonObjectRequest(Request.Method.GET, getUrl(), new JSONObject(), this, this);
        mJSONrequest.setTag(TAG);
        mQueue.add(mJSONrequest);

        mJSONrequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
    }

    public void fetchData() {
//        RequestQueue mQueue;
//        JsonRequest mJSONrequest;
//        mQueue = Volley.newRequestQueue(mContext.getApplicationContext());
//        mJSONrequest = new JsonObjectRequest(Request.Method.GET, getUrl(), new JSONObject(), this, this);
//        mJSONrequest.setTag(TAG);
//        mQueue.add(mJSONrequest);
//
//        mJSONrequest.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {
//                return 50000;
//            }
//
//            @Override
//            public int getCurrentRetryCount() {
//                return 50000;
//            }
//
//            @Override
//            public void retry(VolleyError error) throws VolleyError {
//
//            }
//        });
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        error.printStackTrace();
    }

    @Override
    public void onResponse(Object response) {
        RenderData renderData = new RenderData(mActivity, mContext);
        renderData.renderWeather((JSONObject) response);
        renderData.update();
    }

    //Get the URL String for JSON request
    private String getUrl() {

        Geocoder geoCoder = new Geocoder(mActivity);

        //Getting string from Shared preferences and setting default
        String address = new CityPreference(mActivity).getCity();
        if (address.equals("")) {
            address = mContext.getString(R.string.ns);
        }

        List<Address> addressList = null;
        try {
            addressList = geoCoder.getFromLocationName(address, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Check if user entered invalid name of city (not Working)
        String urlDarkSky = "";
        if (addressList.size() != 0) {
            double lat = addressList.get(0).getLatitude();
            double lng = addressList.get(0).getLongitude();

            //Create our own location
            Location loc = new Location("");
            loc.setLatitude(lat);
            loc.setLongitude(lng);

            // Make  url string for request
            StringBuilder stringBuilderDarkSkyUrl = new StringBuilder();
            stringBuilderDarkSkyUrl.append(mContext.getString(R.string.api));
            stringBuilderDarkSkyUrl.append(loc.getLatitude());
            stringBuilderDarkSkyUrl.append(",");
            stringBuilderDarkSkyUrl.append(loc.getLongitude());
            urlDarkSky = stringBuilderDarkSkyUrl.toString(); //urlPartOneFromDarkSky + Double.toString(loc.getLatitude()) + "," + Double.toString(loc.getLatitude());
        } else {
            Toast toast = Toast.makeText(mContext, mActivity.getString(R.string.wrong_city_name),
                    Toast.LENGTH_LONG);
            toast.show();
        }
        return urlDarkSky;
    }


}