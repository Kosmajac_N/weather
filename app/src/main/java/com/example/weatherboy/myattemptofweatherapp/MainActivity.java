package com.example.weatherboy.myattemptofweatherapp;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;

import android.content.SharedPreferences;

import android.net.ConnectivityManager;

import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import android.widget.EditText;
import android.widget.RadioGroup;

import com.example.weatherboy.myattemptofweatherapp.Adapter.SectionsPagerAdapter;
import com.example.weatherboy.myattemptofweatherapp.Fragments.ChangeBackgroundFragment;
import com.example.weatherboy.myattemptofweatherapp.Networking.RemoteFetch;
import static com.example.weatherboy.myattemptofweatherapp.Fragments.ChangeBackgroundFragment.FRAG;


public class MainActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener, ViewPager.OnPageChangeListener{

    SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;
    RadioGroup mRadioGroup;
    public  Activity mActivity;
    public  Context mContext;

    SharedPreferences backgroundPrefs;
    android.support.design.widget.CoordinatorLayout mainFrame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainFrame = (CoordinatorLayout) findViewById(R.id.main_content);
        if(new CityPreference(this).getBackground() != 0){
            mainFrame.setBackgroundResource(new CityPreference(this).getBackground());

        }
        mContext = getApplicationContext();
        mActivity = this;

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(this);

        mRadioGroup = (RadioGroup) findViewById(R.id.page_group);
        mRadioGroup.setOnCheckedChangeListener(this);



    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!isNetworkAvailable(getApplicationContext())) {
            alertDialogue();
        }
        if (isNetworkAvailable(this))
            new RemoteFetch(this, this).run();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public void alertDialogue() {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle(R.string.no_network);
        alertDialog.setMessage(getResources().getString(R.string.turn_on_your_internet));
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
//                        Intent i = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
//                        startActivity(i);
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(this.getApplicationContext()).inflate(R.menu.weather, menu);
        return true;
    }

    /**
     * Creating Dialogue for Changing City
     *
     * @param menu     change city
     * @param inflater dialogue
     */


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.change_city) {
            showInputDialog();
        } else if (item.getItemId() == R.id.change_background) {

            //opens new fragment for selecting background
            DialogFragment fragment = ChangeBackgroundFragment.instanceOfBackgroundFrag(1);
            fragment.show(getSupportFragmentManager(), FRAG);

        }
        return false;
    }

    public void showInputDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.change_city);
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton(R.string.go, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                changeCity(input.getText().toString().replace(" ", ""));
                //TODO Make a thread for this process?
//                new Thread(new Runnable() {
//                    public void run() {
//
//                        new RemoteFetch(mContext, mActivity).fetchData();
//                    }
//                }).start();
                new RemoteFetch(mContext, mActivity).run();
            }
        });
        builder.show();
    }

    public void changeCity(String city) {
        new CityPreference(this).setCity(city);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        // when checked radio button -> update current page
        mViewPager.setCurrentItem(mRadioGroup.indexOfChild(mRadioGroup.findViewById(checkedId)), true);

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        // when current page change -> update radio button state
        int radioButtonId = mRadioGroup.getChildAt(position).getId();
        mRadioGroup.check(radioButtonId);

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}