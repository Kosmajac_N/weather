package com.example.weatherboy.myattemptofweatherapp.Networking;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.weatherboy.myattemptofweatherapp.Fragments.WeatherMainFragment;
import com.example.weatherboy.myattemptofweatherapp.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * Created by nikola on 2.2.17..
 */

public class RenderData {
    private Activity mActivity;
    private Context mContext;

    //main screen
    private String iconMain;
    private double temperature;
    private double humidity;
    private String pressure;
    private String stateName;
    private String countryName;
    private String updatedOn;
    private String summary;

    List<Day> daysList;


    public RenderData(Activity a, Context c) {
        mActivity = a;
        mContext = c;
        daysList = new ArrayList<Day>();
    }


    public void renderWeather(JSONObject json) {

        try {
            double latitude = json.getDouble("latitude");
            double longitude = json.getDouble("longitude");
            JSONObject hourly = json.getJSONObject("hourly");
            JSONArray data = hourly.getJSONArray("data");

            JSONObject currently = json.getJSONObject("currently");
            long time = currently.getLong("time");


            JSONObject record = (JSONObject) data.get(0);
            double temperature = record.getDouble("temperature");


            //Get the name of the city from latitude and longitude and show it to the user
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            //String cityName = addresses.get(0).getAddressLine(0); //unnecessary string, and it is never used
            String stateName = addresses.get(0).getAddressLine(1);
            String countryName = addresses.get(0).getAddressLine(2);
            if (countryName == null)
                countryName = "";

            setStateName(stateName);
            setCountryName(countryName);
            setTemperature(temperature);

            //Show last update to user
            DateFormat df = DateFormat.getDateTimeInstance();
            String updatedOn = df.format(new Date(time * 1000));
            setUpdateField(updatedOn);


            JSONObject details = json.getJSONObject("currently");

            double humidity = details.getDouble("humidity");
            String pressure = (mActivity.getString(R.string.pressure) + details.getString("pressure") + mActivity.getString(R.string.hpa));
            humidity = humidity * 100;

            //Setting the main weather icon
            String iconFromDarkSky = details.getString("icon");
            setIconMain(iconFromDarkSky);
            setIconDarkSky(iconFromDarkSky);

            //Setting up the details for main fragment
            String summary = details.getString("summary");
            setSummary(summary);
            setPressure(pressure);
            setHumidity(humidity);

            //Getting data for  DAY
            JSONObject daily = json.getJSONObject("daily");
            JSONArray dailyData = daily.getJSONArray("data");

            for (int i = 0; i<dailyData.length(); i++) {
                JSONObject day = (JSONObject) dailyData.get(i);
                double tempMax = day.getDouble("temperatureMax");
                double tempMin = day.getDouble("temperatureMin");
                long timeDay = day.getLong("time");
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(timeDay * 1000);
                int dayIndex = cal.get(Calendar.DAY_OF_WEEK);
                String icon = day.getString("icon");
                setBackground(icon);
                String summaryFirstDay = day.getString("summary");

                daysList.add(new Day(tempMax, tempMin, icon, getDay(dayIndex), summaryFirstDay  ));

            }





        } catch (Exception e) {
            Log.e("SimpleWeather", "One or more fields not found in the JSON data");
        }
    }

    //Setting up fields for main screen
    public void setStateName(String stateName) {
        this.stateName = stateName.replaceAll("\\d", "");
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName.replaceAll("\\d", "");
    }

    private void setIconMain(String icon) {
        iconMain = icon;
    }

    private void setTemperature(double temp) {
        temperature = temp;
    }

    private void setUpdateField(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    private void setPressure(String pressure) {
        this.pressure = pressure;
    }

    private void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    private void setSummary(String summary) {
        this.summary = summary;
    }




    /***
     * Updating Views in Weather Fragment
     */
    public void update() {
        TextView cityField = (TextView) (mActivity).findViewById(R.id.city_field);
        cityField.setText(stateName + ", " + countryName);
        TextView currentTempField = (TextView) (mActivity).findViewById(R.id.current_temperature_field);
        currentTempField.setText(String.format("%.0f", FahrenheitToCelsius(temperature)) + " ℃");
        TextView updatedField = (TextView) (mActivity).findViewById(R.id.updated_field);
        updatedField.setText(updatedOn);
        TextView details = (TextView) (mActivity).findViewById(R.id.details_field);
        details.setText(summary.toUpperCase(Locale.US) + "\n" + "Humidity: " + String.format("%.0f", humidity) + "%" +
                "\n" + pressure);

        TextView tvIconMain = (TextView) (mActivity).findViewById(R.id.weather_icon);
        tvIconMain.setText(setIconDarkSky(iconMain));

        //Update FIVE DAYS FRAGMENT

        //Day One
        TextView tempMax = (TextView) (mActivity).findViewById(R.id.tvMaxTempDayOne);
        tempMax.setText(String.format("%.0f", FahrenheitToCelsius(daysList.get(1).getTempMax())) + (char) 0x00B0);


        ImageView tvIcon = (ImageView) (mActivity).findViewById(R.id.weather_icon_day_one);
        tvIcon.setImageDrawable(setBackground(daysList.get(1).getIcon()));

        TextView tempMin2 = (TextView) mActivity.findViewById(R.id.tvMinTempDayOne);
        tempMin2.setText(String.format("%.0f", FahrenheitToCelsius(daysList.get(1).getTempMin())) + (char) 0x00B0);

        TextView day = (TextView) mActivity.findViewById(R.id.tvDayOne);
        day.setText(daysList.get(1).getDayOfWeek());

        TextView summary = (TextView) (mActivity).findViewById(R.id.tvForcastTextDayOne);
        summary.setText(daysList.get(1).getSummary());




        //Day Two
        TextView tempMaxSecondDay = (TextView) (mActivity).findViewById(R.id.tvMaxTempDayTwo);
        tempMaxSecondDay.setText(String.format("%.0f", FahrenheitToCelsius(daysList.get(2).getTempMax())) + (char) 0x00B0);


        ImageView tvIconSecondDay = (ImageView) (mActivity).findViewById(R.id.weather_icon_day_two);
        tvIconSecondDay.setImageDrawable(setBackground(daysList.get(2).getIcon()));

        TextView tempMin2SecondDay = (TextView) mActivity.findViewById(R.id.tvMinTempDayTwo);
        tempMin2SecondDay.setText(String.format("%.0f", FahrenheitToCelsius(daysList.get(2).getTempMin())) + (char) 0x00B0);

        TextView daySecondDay = (TextView) mActivity.findViewById(R.id.tvDayTwo);
        daySecondDay.setText(daysList.get(2).getDayOfWeek());

        TextView summarySecondDay = (TextView) (mActivity).findViewById(R.id.tvForcastTextDayTwo);
        summarySecondDay.setText(daysList.get(2).getSummary());




        //Day Three
        TextView tempMaxThirdDay = (TextView) (mActivity).findViewById(R.id.tvMaxTempDayThree);
        tempMaxThirdDay.setText(String.format("%.0f", FahrenheitToCelsius(daysList.get(3).getTempMax())) + (char) 0x00B0);


        ImageView tvIconThirdDay = (ImageView) (mActivity).findViewById(R.id.weather_icon_day_three);
        tvIconThirdDay.setImageDrawable(setBackground(daysList.get(3).getIcon()));

        TextView tempMin2ThirdDay = (TextView) mActivity.findViewById(R.id.tvMinTempDayThree);
        tempMin2ThirdDay.setText(String.format("%.0f", FahrenheitToCelsius(daysList.get(3).getTempMin())) + (char) 0x00B0);

        TextView dayThirdDay = (TextView) mActivity.findViewById(R.id.tvDayThree);
        dayThirdDay.setText(daysList.get(3).getDayOfWeek());

        TextView summaryThirdDay = (TextView) (mActivity).findViewById(R.id.tvForcastTextDayThree);
        summaryThirdDay.setText(daysList.get(3).getSummary());



        //Day Four
        TextView tempMaxFourthDay = (TextView) (mActivity).findViewById(R.id.tvMaxTempDayFour);
        tempMaxFourthDay.setText(String.format("%.0f", FahrenheitToCelsius(daysList.get(4).getTempMax())) + (char) 0x00B0);


        ImageView tvIconFourthDay = (ImageView) (mActivity).findViewById(R.id.weather_icon_day_four);
        tvIconFourthDay.setImageDrawable(setBackground(daysList.get(4).getIcon()));

        TextView tempMinFourthDay4 = (TextView) mActivity.findViewById(R.id.tvMinTempDayFour);
        tempMinFourthDay4.setText(String.format("%.0f", FahrenheitToCelsius(daysList.get(4).getTempMin())) + (char) 0x00B0);

        TextView dayFourthDay = (TextView) mActivity.findViewById(R.id.tvDayFour);
        dayFourthDay.setText(daysList.get(4).getDayOfWeek());

        TextView summaryFourthDay = (TextView) (mActivity).findViewById(R.id.tvForcastTextDayFour);
        summaryFourthDay.setText(daysList.get(4).getSummary());


        //Day Five
        TextView tempMaxFifthDay = (TextView) (mActivity).findViewById(R.id.tvMaxTempDayFive);
        tempMaxFifthDay.setText(String.format("%.0f", FahrenheitToCelsius(daysList.get(5).getTempMax())) + (char) 0x00B0);


        ImageView tvIconFifthDay = (ImageView) (mActivity).findViewById(R.id.weather_icon_day_five);
        tvIconFifthDay.setImageDrawable(setBackground(daysList.get(5).getIcon()));

        TextView tempMinFifthDay5 = (TextView) mActivity.findViewById(R.id.tvMinTempDayFive);
        tempMinFifthDay5.setText(String.format("%.0f", FahrenheitToCelsius(daysList.get(5).getTempMin())) + (char) 0x00B0);

        TextView dayFifthDay = (TextView) mActivity.findViewById(R.id.tvDayFive);
        dayFifthDay.setText(daysList.get(5).getDayOfWeek());

        TextView summaryFifthDay = (TextView) (mActivity).findViewById(R.id.tvForcastTextDayFive);
        summaryFifthDay.setText(daysList.get(5).getSummary());





    }

    private String setIconDarkSky(String received_icon) {
        String icon = "";
        switch (received_icon) {
            case "clear-day":
                icon = mContext.getString(R.string.weather_sunny);
                break;
            case "clear-night":
                icon = mContext.getString(R.string.weather_clear_night);
                break;
            case "thunderstorm":
                icon = mContext.getString(R.string.weather_thunder);
                break;
            case "sleet":
                icon = mContext.getString(R.string.weather_drizzle);
                break;
            case "fog":
                icon = mContext.getString(R.string.weather_foggy);
                break;
            case "cloudy":
                icon = mContext.getString(R.string.weather_cloudy);
                break;
            case "snow":
                icon = mContext.getString(R.string.weather_snowy);
                break;
            case "rain":
                icon = mContext.getString(R.string.weather_rainy);
                break;
            case "partly-cloudy-day":
                icon = mContext.getString(R.string.weather_cloudy);
                break;
            case "partly-cloudy-night":
                icon = mContext.getString(R.string.weather_cloudy);
                break;
        }
        return icon;
    }

    private Drawable setBackground(final String iconFromDarkSky) {

        Drawable imageForecast = null;
        switch (iconFromDarkSky) {
            case "clear-day":
                imageForecast = ContextCompat.getDrawable(mActivity, R.drawable.sun);
                break;

            case "clear-night":
                imageForecast = ContextCompat.getDrawable(mActivity, R.drawable.night_icon);
                break;

            case "thunderstorm":
                imageForecast = ContextCompat.getDrawable(mActivity, R.drawable.thunder_icon);
                break;

            case "sleet":
                imageForecast = ContextCompat.getDrawable(mActivity, R.drawable.sleet_icon);
                break;

            case "fog":
                imageForecast = ContextCompat.getDrawable(mActivity, R.drawable.foggy_icon);
                break;

            case "cloudy":
                imageForecast = ContextCompat.getDrawable(mActivity, R.drawable.cloud);
                break;

            case "snow":
                imageForecast = ContextCompat.getDrawable(mActivity, R.drawable.snowing_icon);
                break;

            case "rain":
                imageForecast = ContextCompat.getDrawable(mActivity, R.drawable.rain_icon);
                break;

            case "partly-cloudy-day":
                imageForecast = ContextCompat.getDrawable(mActivity, R.drawable.partly_cloudy_icon);
                break;

            case "partly-cloudy-night":
                imageForecast = ContextCompat.getDrawable(mActivity, R.drawable.partly_cloudy_night_icon);
                break;
        }
        return imageForecast;
    }

    //  Converting Fahrenheit
    private double FahrenheitToCelsius(double tempInFarenheit) {
        return ((tempInFarenheit - 32) * 5) / 9;
    }

    private String getDay(int dayNumber) {
        String day = "";
        switch (dayNumber) {
            case 1:
                day = mActivity.getString(R.string.sunday);
                break;
            case 2:
                day = mActivity.getString(R.string.monday);
                break;
            case 3:
                day = mActivity.getString(R.string.tuesday);
                break;
            case 4:
                day = mActivity.getString(R.string.wednesday);
                break;
            case 5:
                day = mActivity.getString(R.string.thursday);
                break;
            case 6:
                day = mActivity.getString(R.string.friday);
                break;
            case 7:
                day = mActivity.getString(R.string.saturday);
        }
        return day;
    }
}
