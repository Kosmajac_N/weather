package com.example.weatherboy.myattemptofweatherapp.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.weatherboy.myattemptofweatherapp.Fragments.NextFiveDays;
import com.example.weatherboy.myattemptofweatherapp.Fragments.WeatherMainFragment;

/**
 * Created by nikola on 1.2.17..
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return WeatherMainFragment.newInstanceOfMainFrag(position);
            case 1:
                return NextFiveDays.instanceofNextFiveDays(position);
        }
        return WeatherMainFragment.newInstanceOfMainFrag(position + 1);
    }

    @Override
    public int getCount() {
        return 2;
    }

    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 1:
                return "First Fragment";

            case 2:
                return "Second Fragment";

            case 3:
                return "Third Fragment";

        }
        return null;
    }
}
