package com.example.weatherboy.myattemptofweatherapp.Fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.example.weatherboy.myattemptofweatherapp.Adapter.RecyclerAdapter;
import com.example.weatherboy.myattemptofweatherapp.R;



/**
 * Created by nikola on 14.2.17..
 */

public class ChangeBackgroundFragment extends DialogFragment {

    public static final String TAG = "background";
    public static final String FRAG = "options_fragment";
    private RecyclerView mRecycleView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;

    public ChangeBackgroundFragment() {
    }

    public static ChangeBackgroundFragment instanceOfBackgroundFrag(int sectionNumber) {
        ChangeBackgroundFragment fragment = new ChangeBackgroundFragment();
        Bundle args = new Bundle();
        args.putInt(FRAG, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setStyle(STYLE_NO_FRAME, android.R.style.Theme_Holo_Light);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.backround_options, container, false);
        mRecycleView = (RecyclerView) rootView.findViewById(R.id.rvImages);
        mRecycleView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mRecycleView.setLayoutManager(mLayoutManager);
        new RecyclerAdapter(getContext(),this).fillItems();
        mAdapter = new RecyclerAdapter(getContext(),this);
        mRecycleView.setAdapter(mAdapter);
        return rootView;
    }


    public void changePreviewBackground(int pos) {
        Dialog dialogView = this.getDialog();
        RelativeLayout rl = (RelativeLayout) dialogView.findViewById(R.id.background_options_layout);
        switch (pos) {
            case 0:
                rl.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.black_background));
                break;
            case 1:
                rl.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.blue_shadow_background));
                break;
            case 2:
                rl.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.purple_background));
                break;
            case 3:
                rl.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.cool_abstract_background));
                break;
            case 4:
                rl.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.blue_dark_background));
                break;
            case 5:
                rl.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.green_background));
                break;
            case 6:
                rl.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.sunshine_yellow_deep_backgound));
                break;
        }
    }
}
