package com.example.weatherboy.myattemptofweatherapp.Networking;

/**
 * Created by nikola on 23.2.17..
 */

public class Day {

    private double tempMax;
    private String icon;
    private double tempMin;
    private String dayOfWeek;
    private String summary;


    public Day(double tempMax, double tempMin, String icon, String dayOfWeek, String summary) {
        this.tempMax = tempMax;
        this.tempMin = tempMin;
        this.icon = icon;
        this.dayOfWeek = dayOfWeek;
        this.summary = summary;

    }

    public double getTempMax() {
        return tempMax;
    }

    public void setTempMax(double tempMax) {
        this.tempMax = tempMax;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public double getTempMin() {
        return tempMin;
    }

    public void setTempMin(double tempMin) {
        this.tempMin = tempMin;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
