package com.example.weatherboy.myattemptofweatherapp.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import com.example.weatherboy.myattemptofweatherapp.CityPreference;
import com.example.weatherboy.myattemptofweatherapp.Fragments.ChangeBackgroundFragment;
import com.example.weatherboy.myattemptofweatherapp.Fragments.NextFiveDays;
import com.example.weatherboy.myattemptofweatherapp.R;

import static com.example.weatherboy.myattemptofweatherapp.Fragments.ChangeBackgroundFragment.FRAG;

/**
 * Created by nikola on 15.2.17..
 */

public class ChangeMainBackground {


   private Context mContext;
    public static final String BACKGROUND = "Background";

   SharedPreferences sharedPreferences;

    public ChangeMainBackground(Context c) {
        mContext = c;

    }


    public void prefsChangeBackground(int back) {
        new CityPreference((Activity)mContext).setBackground(back);
    }


    public void changeBackground(int pos) {




        CoordinatorLayout frameLayout = (CoordinatorLayout) ((Activity)mContext).findViewById(R.id.main_content);

        switch (pos) {
            case 0:
                frameLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.black_background));
                prefsChangeBackground(R.drawable.black_background);
                break;
            case 1:
                frameLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.blue_shadow_background));
                prefsChangeBackground(R.drawable.black_background);
                break;
            case 2:
                frameLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.purple_background));
                prefsChangeBackground(R.drawable.purple_background);
                break;
            case 3:
                frameLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.cool_abstract_background));
                prefsChangeBackground(R.drawable.cool_abstract_background);
                break;
            case 4:
                frameLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.blue_dark_background));
                prefsChangeBackground(R.drawable.blue_dark_background);
                break;
            case 5:
                frameLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.green_background));
                prefsChangeBackground(R.drawable.green_background);
                break;
            case 6:
                frameLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.sunshine_yellow_deep_backgound));
                prefsChangeBackground(R.drawable.sunshine_yellow_deep_backgound);
                break;
        }
    }


//    public void changePreviewBackground(int pos) {
//
//        View rootview = (View)((Activity)mContext).findViewById(R.id.background_options_layout);
//
//
//        //LinearLayout rl = (LinearLayout) ((Activity)mContext).findViewById(R.id.background_options_layout);
//
//        switch (pos) {
//            case 0:
//                DialogFragment fragment = ChangeBackgroundFragment.instanceOfBackgroundFrag(1);
//                fragment.show();
//                break;
//            case 1:
//                rootview.setBackground(mContext.getResources().getDrawable(R.drawable.sunshine_yellow_deep_backgound));
//                break;
//            case 2:
//                rootview.setBackground(ContextCompat.getDrawable(mContext, R.drawable.purple_background));
//                break;
//            case 3:
//                rootview.setBackground(ContextCompat.getDrawable(mContext, R.drawable.cool_abstract_background));
//                break;
//            case 4:
//                rootview.setBackground(ContextCompat.getDrawable(mContext, R.drawable.blue_dark_background));
//                break;
//            case 5:
//                rootview.setBackground(ContextCompat.getDrawable(mContext, R.drawable.green_background));
//                break;
//            case 6:
//                rootview.setBackground(ContextCompat.getDrawable(mContext, R.drawable.sunshine_yellow_deep_backgound));
//                break;
//        }
//    }

public void changeFiveDaysFragmentBackground(int pos){

    LinearLayout ly = (LinearLayout) ((Activity)mContext).findViewById(R.id.fiveDaysLinearLayout);

    switch (pos) {
        case 0:

            ly.setBackground(mContext.getResources().getDrawable(R.drawable.sunshine_yellow_deep_backgound));
            break;
        case 1:
            ly.setBackground(ContextCompat.getDrawable(mContext, R.drawable.blue_shadow_background));
            break;
        case 2:
            ly.setBackground(ContextCompat.getDrawable(mContext, R.drawable.purple_background));
            break;
        case 3:
            ly.setBackground(ContextCompat.getDrawable(mContext, R.drawable.cool_abstract_background));
            break;
        case 4:
            ly.setBackground(ContextCompat.getDrawable(mContext, R.drawable.blue_dark_background));
            break;
        case 5:
            ly.setBackground(ContextCompat.getDrawable(mContext, R.drawable.green_background));
            break;
        case 6:
            ly.setBackground(ContextCompat.getDrawable(mContext, R.drawable.sunshine_yellow_deep_backgound));
            break;
    }

}

}
