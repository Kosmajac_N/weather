package com.example.weatherboy.myattemptofweatherapp.Adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.example.weatherboy.myattemptofweatherapp.Fragments.ChangeBackgroundFragment;
import com.example.weatherboy.myattemptofweatherapp.R;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nikola on 16.2.17..
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    Context mContext;
    List<ItemList> items;


    ChangeBackgroundFragment fragment;


    public RecyclerAdapter( Context c, ChangeBackgroundFragment fragment) {
        mContext = c;
        this.fragment = fragment;
        fillItems();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.mBackground.setImageResource(items.get(position).getBackgroundResource());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ChangeMainBackground(mContext).changeBackground(position);
                fragment.changePreviewBackground(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void fillItems() {

        items = new ArrayList<ItemList>();

        items.add(new ItemList(R.drawable.preview_black));
        items.add(new ItemList(R.drawable.preview_blue_shadow));
        items.add(new ItemList(R.drawable.preview_purple));
        items.add(new ItemList(R.drawable.preview_cool));
        items.add(new ItemList(R.drawable.preview_dark));
        items.add(new ItemList(R.drawable.preview_green));
        items.add(new ItemList(R.drawable.preview_yellow));
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mBackground;

        ViewHolder(View itemView) {
            super(itemView);
            mBackground = (ImageView) itemView.findViewById(R.id.iv_background_item);
        }
    }

    public class ItemList {

        int backgroundResource;

        public ItemList(int backgroundResource) {

            this.backgroundResource = backgroundResource;
        }

        public int getBackgroundResource() {
            return backgroundResource;
        }

        public void setBackgroundResource(int iconResource) {
            this.backgroundResource = iconResource;
        }
    }
}
