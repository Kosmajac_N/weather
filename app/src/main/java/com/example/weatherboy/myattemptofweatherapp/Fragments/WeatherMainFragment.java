package com.example.weatherboy.myattemptofweatherapp.Fragments;

import android.support.v4.app.Fragment;

import android.graphics.Typeface;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.RelativeLayout;
import android.widget.TextView;

//import com.example.weatherboy.myattemptofweatherapp.Networking.RemoteFetch;
//import com.example.weatherboy.myattemptofweatherapp.Networking.RenderData;
import com.example.weatherboy.myattemptofweatherapp.R;


/**
 * Created by nikola on 24.1.17..
 */

public class WeatherMainFragment extends Fragment {

    public TextView cityField;
    public TextView updatedField;
    public TextView detailsField;
    public TextView currentTemperatureField;
    public TextView weatherIcon;

    public Typeface weatherFont;

    public RelativeLayout weatherMainLayout;


    public static final String TAG = "my request";

    public static final String FRAG = "main_fragmetnt";

    public static WeatherMainFragment newInstanceOfMainFrag(int sectionNumber) {
        WeatherMainFragment fragment = new WeatherMainFragment();
        Bundle args = new Bundle();
        args.putInt(FRAG, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        weatherFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/weather.ttf");
        //   updateWeatherData(new CityPreference(getActivity()).getCity());

    }

    /**
     * Creating Dialogue for Changing City
     *
     * @param menu     change city
     * @param inflater dialogue
     */
//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        new MenuInflater(getActivity().getApplicationContext()).inflate(R.menu.weather, menu);
//
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == R.id.change_city) {
//            showInputDialog();
//        }
//        return false;
//    }
//
//    private void showInputDialog() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle(R.string.change_city);
//        final EditText input = new EditText(getActivity());
//        input.setInputType(InputType.TYPE_CLASS_TEXT);
//        builder.setView(input);
//        builder.setPositiveButton(R.string.go, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                changeCity(input.getText().toString().replace(" ", ""));
//
//                //TODO Make a thread for this process
//                new RemoteFetch(getContext(), getActivity()).fetchData();
//            }
//        });
//        builder.show();
//    }
//
//    public void changeCity(String city) {
//        new CityPreference(getActivity()).setCity(city);
//    }












    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_weather, container, false);
        setLayout(rootView);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
//        new Thread(){
//            @Override
//            public void run() {
//                try{
//                    sleep(500);
//                }catch(InterruptedException e){
//                    e.printStackTrace();
//                }finally{
//
//                    new RemoteFetch(getActivity().getApplicationContext(), getActivity()).fetchData();
//
//                }
//            }
//        }.start();
       // new RemoteFetch(getContext(), getActivity()).fetchData();
    }


    /**
     * FETCHING DATA
     */
//    public void fetchData() {
//        Geocoder geoCoder = new Geocoder(getActivity().getApplicationContext());  //bilo getActovity.getApplicationContext()
//
//        //Getting string from Shared preferences and setting default
//        String address = new CityPreference(getActivity()).getCity();
//        if (address.equals("")) {
//            address = getString(R.string.ns);
//        }
//
//        try {
//            List<Address> addressList = null;
//            addressList = geoCoder.getFromLocationName(address, 1);
//
//            //Check if user entered invalid name of city
//            if (addressList.size() != 0) {
//                double lat = addressList.get(0).getLatitude();
//                double lng = addressList.get(0).getLongitude();
//
//
//                //Create our own location
//                Location loc = new Location("");
//                loc.setLatitude(lat);
//                loc.setLongitude(lng);
//
//
//                // Make  url string for request
//                StringBuilder stringBuilderDarkSkyUrl = new StringBuilder();
//                stringBuilderDarkSkyUrl.append(getString(R.string.api));
//                stringBuilderDarkSkyUrl.append(loc.getLatitude());
//                stringBuilderDarkSkyUrl.append(",");
//                stringBuilderDarkSkyUrl.append(loc.getLongitude());
//                String urlDarkSky = stringBuilderDarkSkyUrl.toString(); //urlPartOneFromDarkSky + Double.toString(loc.getLatitude()) + "," + Double.toString(loc.getLatitude());
//
//                // Instantiate the RequestQueue
//
//                mQueue = Volley.newRequestQueue(getActivity().getApplicationContext()); //umesto this je bilo getApllicationContext();
//                myRequestDarkSky = new JsonObjectRequest(Request.Method.GET, urlDarkSky, new JSONObject(), this, this);
//                myRequestDarkSky.setTag(TAG);
//            } else {
//                showInputDialog();
//            }
//            // Add the request to the RequestQueue
//            mQueue.add(myRequestDarkSky);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }


//    @Override
//    public void onResponse(Object response) {
//        new RenderData(getActivity(),getContext()).renderWeather((JSONObject)response);
//
//    }

    /**
     * Rendering data from JSON and setting up the fields
     */
//
//    public void renderWeather(JSONObject json) {
//
//
//        try {
//            double latitude = json.getDouble("latitude");
//            double longitude = json.getDouble("longitude");
//            JSONObject hourly = json.getJSONObject("hourly");
//            JSONArray data = hourly.getJSONArray("data");
//
//            JSONObject currently = json.getJSONObject("currently");
//            long time = currently.getLong("time");
//
//            JSONObject record = (JSONObject) data.get(0);
//            double temperature = record.getDouble("temperature");

//            currentTemperatureField.setText(
//                    String.format("%.1f", FahrenheitToCelsius(temperature)) + " ℃");

//            //Get the name of the city from latitude and longitude and show it to the user
//            Geocoder geocoder = new Geocoder(getActivity().getApplicationContext(), Locale.getDefault());
//            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
//            //String cityName = addresses.get(0).getAddressLine(0); //unnecessary string, and it is never used
//            String stateName = addresses.get(0).getAddressLine(1);
//            String countryName = addresses.get(0).getAddressLine(2);
//
//            cityField.setText(stateName.replaceAll("\\d", "") + ", " + countryName.replaceAll("\\d", ""));
//
//            DateFormat df = DateFormat.getDateTimeInstance();
//            String updatedOn = df.format(new Date(time * 1000));
//            updatedField.setText(getString(R.string.last_update) + updatedOn);
//
//
//            JSONObject details = json.getJSONObject("currently");
//
//            double humidity = details.getDouble("humidity");
//            humidity = humidity * 100;
//            detailsField.setText(
//                    details.getString("summary").toUpperCase(Locale.US) +
//                            "\n" + getString(R.string.humidity) + String.format("%.2f", humidity) + getString(R.string.percentage) +
//                            "\n" + getString(R.string.pressure) + details.getString("pressure") + getString(R.string.hpa));
//
//            final String iconFromDarkSky = details.getString("icon");
//            setIconDarkSky(iconFromDarkSky);
//
//         //   setBackground(iconFromDarkSky);
//
//
//        } catch (Exception e) {
//            Log.e(getString(R.string.myApp), getString(R.string.error_no_fields));
//        }
//    }

    private void setIconDarkSky(String received_icon) {

        String icon = "";
        switch (received_icon) {
            case "clear-day":
                icon = getString(R.string.weather_sunny);
                break;

            case "clear-night":
                icon = getString(R.string.weather_clear_night);
                break;

            case "thunderstorm":
                icon = getString(R.string.weather_thunder);
                break;

            case "sleet":
                icon = getString(R.string.weather_drizzle);
                break;

            case "fog":
                icon = getString(R.string.weather_foggy);
                break;

            case "cloudy":
                icon = getString(R.string.weather_cloudy);
                break;

            case "snow":
                icon = getString(R.string.weather_snowy);
                break;

            case "rain":
                icon = getString(R.string.weather_rainy);
                break;

            case "partly-cloudy-day":
                icon = getString(R.string.weather_cloudy);
                break;

            case "partly-cloudy-night":
                icon = getString(R.string.weather_cloudy);
                break;
        }
        weatherIcon.setText(icon);

    }

//    @Override
//    public void onErrorResponse(VolleyError error) {
//        updatedField.setText(error.getMessage());
//    }

    private void setLayout(View v) {
        cityField = (TextView) v.findViewById(R.id.city_field);
        updatedField = (TextView) v.findViewById(R.id.updated_field);
        detailsField = (TextView) v.findViewById(R.id.details_field);
        currentTemperatureField = (TextView) v.findViewById(R.id.current_temperature_field);
        weatherIcon = (TextView) v.findViewById(R.id.weather_icon);
        weatherIcon.setTypeface(weatherFont);
        weatherMainLayout = (RelativeLayout) v.findViewById(R.id.weather_layout);

//        tvNextFiveDays.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentTransaction ft = getFragmentManager().beginTransaction();
//                ft.replace(R.id.container, new NextFiveDays()).commit();
//            }
//        });
    }

//    public void updapteFields(){
//        cityField.setText(stateName);
//    }


    //not sure wether to use this or not... probably not!
//    public void updateLayout(){
//        RenderData rd = new RenderData(getActivity(), getContext());
//        cityField.setText(rd.getStateName());
//    }
}