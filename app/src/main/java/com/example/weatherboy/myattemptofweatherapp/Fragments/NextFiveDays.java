package com.example.weatherboy.myattemptofweatherapp.Fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.weatherboy.myattemptofweatherapp.R;


/**
 * Created by nikola on 27.1.17..
 */

public class NextFiveDays extends Fragment {

    public static final String ARGS = "next_five_days";

    public static NextFiveDays instanceofNextFiveDays(int sectionNumber) {
        NextFiveDays fragment = new NextFiveDays();
        Bundle args = new Bundle();
        args.putInt(ARGS, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.five_days_forecast, container, false);
        return v;
    }
}
