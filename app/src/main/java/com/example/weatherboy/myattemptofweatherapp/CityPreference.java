package com.example.weatherboy.myattemptofweatherapp;

import android.app.Activity;
import android.content.SharedPreferences;


/**
 * Created by nikola on 16.1.17..
 */

public class CityPreference {

    SharedPreferences prefs;

    public CityPreference(Activity activity) {
        prefs = activity.getPreferences(Activity.MODE_PRIVATE);
    }

    public String getCity() {
        return prefs.getString("city", "Sydney, AU");
    }

    public void setCity(String city) {
        prefs.edit().putString("city", city).apply(); //ovde je bilo city.toUpperCase()
    }

    public int getBackground(){
        return prefs.getInt("background", R.drawable.blue_background);
    }

    public void setBackground(int background){
        prefs.edit().putInt("background", background).apply();
    }
}
